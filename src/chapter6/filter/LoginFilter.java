package chapter6.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import chapter6.beans.User;

@WebFilter(urlPatterns = { "/setting", "/edit" })
public class LoginFilter implements Filter {


	@Override
	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain chain) throws IOException, ServletException {

	HttpServletResponse httpResponse = (HttpServletResponse) response;
	HttpServletRequest httpRequest = (HttpServletRequest) request;
	HttpSession session = (httpRequest).getSession();

	User user = (User) session.getAttribute("loginUser");

		if(user == null) {
			session.setAttribute("errorMessages", "ログインしてください");
			httpResponse.sendRedirect("./login");
			return;
		}
		chain.doFilter(request, response);
	}

	@Override
	public void init(FilterConfig config) {
	}

	@Override
	public void destroy() {
	}

}
